#+AUTHOR: Emmanuel Manolios
#+TITLE: Documentation for my-compiler 

* General About

This is a compiler for a language designed by Emmanuel Manolios, a current
student at Northeastern University. This compiler uses many ideas introduced
from his compiler course at Northeastern. As a result, many of the ideas 
and features this compiler has are from _Modern Compiler Implementation in ML_
by Andrew Appel, and _Compilers: Principles, Techniques, and Tools_ by 
Alfred Aho, Ravi Sethi, and Jeffrey Ullman, also known as The Dragon Book. 

This compiler is written in Ocaml and is targetted to MIPS. It will contain 
lexical analysis, parsing, AST generation, frame analysis, IR, 
canonicalization, code generation, flow analysis, dataflow analysis, and
register allocation. 


* Lexing

This is done through ocamllex. 

* Parsing

This is done through ocamlyacc. 

* Typechecking

Want to perform type inference as well. 

* Frame Analysis

* Intermediate Representation

* Canonicalization

* Code Generation

A good amount of optimizations will be performed here. 

* Flow Analysis

* Dataflow Analysis

* Register allocation 

Register allocation is done in two primary ways.

** Graph Coloring

This is certainly the more common method of register allocation. We use 
flow and dataflow analysis in order to construct an interference graph, 
which we then color. We attempt to coalesce using multiple algorithms,
but if we are unable to get a satisfactory coloring, we will spill a 
value and try again. Once we get a satisfactory coloring, we will then 
attempt to allocate the least amount of memory for any spilled values.  

** Integer Linear Programming

We also register allocate using integer linear programming, an idea discussed
by Andrew Appel. We use *Inez*, an integer linear programming constraint 
solver written Ocaml in order to achieve this. 
